package com.edu.funix;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 

import com.edu.funix.MyProduct;
import com.edu.funix.entity.Product;

import java.util.Scanner;

/**
 *
 * @author TrongDuyDao
 */
public class Main {
    
    static int getMenuItem() {
        System.out.print("Product list\n1.Insert a new product\n2.In-order traverse"
                + "\n3.Breadth first traverse\n4.Search by a product code"
                + "\n5.Delete by a product code\n6.Simple balancing"
                + "\n7.Count number of products\n0.Exit\nYour choice: ");
        return Integer.valueOf(new Scanner(System.in).nextLine());
    }
    //add some products to the tree
    static void addProducts(MyProduct products) {
        products.insert(new Product("P04", "CD", 10, 2, 0.7));
        products.insert(new Product("P01", "DCD", 7, 8, 1.2));
        products.insert(new Product("P05", "Book", 21, 9, 2.9));
        products.insert(new Product("P02", "Tape", 11, 16, 0.3));
    }
    
    public static void main(String[] args) {
        int c = -1;
        MyProduct products = new MyProduct();
        //add some products to the tree
        addProducts(products);
        while(c != 0) {
            c = getMenuItem();
            switch(c) {
                case 1: products.insert();break;
                case 2: products.inOrder();break;
                case 3: products.BFT();break;
                case 4: products.search();break;
                case 5: products.delete();break;
                case 6: products.balance();break;
                case 7: System.out.println("Number of products " + products.size());break;
                case 0: return;
                default: System.out.println("Invalid choice...");
            }
        }
    }
}
