/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edu.funix;

import com.edu.funix.entity.Product;
import com.edu.funix.util.MyBSTree;
import com.edu.funix.util.Node;

import java.util.Scanner;

/**
 *
 * @author TrongDuyDao
 */
public class MyProduct {
    
    //a list of products
    MyBSTree tree;

    public MyProduct() {
        tree = new MyBSTree();
    }
    
    //1.1 input and insert a new product to tree
    public void insert() {
        Scanner in = new Scanner(System.in);
        //make sure that product code is unique
        String code = "";
        while(true) {
            System.out.print("Enter product code: ");
            if(tree.search(code = in.nextLine()) != null) 
                System.out.println("Product code cannot be duplicated, try again");
            else break;
        }
        System.out.print("Enter product name: ");
        String name = in.nextLine();
        System.out.print("Enter product quantity: ");
        int quantity = Integer.valueOf(in.nextLine());
        System.out.print("Enter product saled: ");
        int saled = Integer.valueOf(in.nextLine());
        System.out.print("Enter product price: ");
        double price = Double.valueOf(in.nextLine());
        //insert to a tree
        tree.insert(new Product(code, name, quantity, saled, price));
        System.out.println("New product has been added");
    }
    //1.1.0 insert a new product to tree (use in main)
    public void insert(Product p) {
        tree.insert(p);
    }
    //1.2 in-order traverse
    public void inOrder() {
        tree.inOrder();
    }
    //1.3 BFT a tree
    public void BFT() {
        tree.BFT();
    }
    //1.4 search a product by product code
    public void search() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter product code to search: ");
        String code = in.nextLine();
        Node<Product> p = tree.search(code);
        if(p == null) System.out.println("Product code " + code + " does not exists");
        else {
            System.out.println("Information of product code " + code);
            System.out.println(p.info);
        }
    }
    //1.5 delete a product by product code
    public void delete() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter product code to delete: ");
        String code = in.nextLine();
        Node<Product> p = tree.search(code);
        if(p == null) System.out.println("Product code " + code + " does not exists");
        else {
            tree.delete(code);
            System.out.println("Product code " + code + " has been deleted");
        }
    }
    //1.6 simply balancing a tree
    public void balance() {
        tree.balance();
    }
    //1.7 count the number of products in the tree
    public int size() {
        return tree.count();
    }
}
