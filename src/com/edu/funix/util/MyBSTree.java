package com.edu.funix.util;

import com.edu.funix.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class MyBSTree {

    //a root of tree
    Node<Product> root;

    public MyBSTree() {
        root = null;
    }

    //visit a node of a tree -> output information of visited node
    public void visit(Node<Product> p) {
        if (p == null) {
            return;
        }
        System.out.println(p.info);
    }

    //return true if tree is empty otherwise return false
    public boolean isEmpty() {
        return root == null;
    }

    //inorder a tree
    private void inOrder(Node<Product> p) {
        if (p == null) {
            return;
        }
        inOrder(p.left);
        visit(p);
        inOrder(p.right);
    }
    
    public void inOrder() {
        if(isEmpty()) return;
        System.out.println(String.format("%-10s%-20s%-10s%-10s%-10s", "Code","Name","Quantity","Saled","Price"));
        inOrder(root);
    }

    //count number of products
    private int count(Node<Product> p) {
        if (p == null) {
            return 0;
        }
        int cnt = 0;
        MyQueue mq = new MyQueue();
        mq.enqueue(p);
        while (!mq.isEmpty()) {
            Node<Product> q = (Node<Product>) mq.dequeue();
            cnt++;
            if (q.left != null) {
                mq.enqueue(q.left);
            }
            if (q.right != null) {
                mq.enqueue(q.right);
            }
        }
        return cnt;
    }

    public int count() {
        return count(root);
    }
    //breath-first traverse a tree
    private void BFT(Node<Product> p) {
        if (p == null) {
            return;
        }
        System.out.println(String.format("%-10s%-20s%-10s%-10s%-10s", "Code","Name","Quantity","Saled","Price"));
        MyQueue mq = new MyQueue();
        mq.enqueue(p);
        while (!mq.isEmpty()) {
            Node<Product> q = (Node<Product>) mq.dequeue();
            System.out.println(q.info);
            if (q.left != null) {
                mq.enqueue(q.left);
            }
            if (q.right != null) {
                mq.enqueue(q.right);
            }
        }
    }

    public void BFT() {
        BFT(root);
    }
    
    //insert a new Product to a tree
    public void insert(Product product) {
        /*TO DO LIST
          * 1. set p = new node of(v)
          * 2. if tree is empty then root = p and exit
          * 3. find f (f is future father of p in tree)
          * 4. attach p to f
         */
        Node<Product> p = new Node<>(product);
        if (isEmpty()) {
            root = p;
            return;
        }
        Node<Product> f = null, q = root;
        //find f
        while (q != null) {
            if (q.info.equals(product)) {
                System.out.println("The key exists, insertion will be canceled here");
                return;
            }
            f = q;
            //go to the right child if q.info < product
            if (q.info.getCode().compareToIgnoreCase(product.getCode()) < 0) {
                q = q.right;
            } else {
                q = q.left;
            }
        }
        //attached p to f, p is left child of f if f.info > p.info
        if (p.info.getCode().compareToIgnoreCase(f.info.getCode()) < 0) {
            f.left = p;
        } else {
            f.right = p;
        }
    }

    //balance a tree
    //step 1: traverse inorder tree and copy all item on tree to an arraylist
    //step 2: insert all items of list to a tree
    private void buildArray(List<Node<Product>> list, Node<Product> p) {
        if (p == null) {
            return;
        }
        buildArray(list, p.left);
        list.add(p);
        buildArray(list, p.right);
    }

    //step 2:
    private void balance(List<Node<Product>> list, int f, int l) {
        if (f > l) {
            return;
        }
        int mid = (f + l) / 2;
        Node<Product> p = list.get(mid);
        insert(p.info);
        balance(list, f, mid - 1);
        balance(list, mid + 1, l);
    }

    public void balance() {
        List<Node<Product>> list = new ArrayList<>();
        buildArray(list, root);
        MyBSTree tree = new MyBSTree();
        tree.balance(list, 0, list.size() - 1);
        root = tree.root;
    }

    //search a Node of tree by product code
    //return null if given code does not exists
    public Node<Product> search(String code) {
        if (isEmpty()) {
            return null;
        }
        Node<Product> p = root;
        while (p != null) {
            if (p.info.getCode().equalsIgnoreCase(code)) {
                break;
            }
            if (p.info.getCode().compareToIgnoreCase(code) < 0) {
                p = p.right;
            } else {
                p = p.left;
            }
        }
        return p;
    }

    //return node father of a given node
    private Node<Product> getFather(Node<Product> p) {
        if (p == root || p == null) {
            return null;
        }
        Node<Product> f = null, t = root;
        while (t != null) {
            if (t.left == p || t.right == p) {
                f = t;
                break;
            } else if (t.info.getCode().compareToIgnoreCase(p.info.getCode()) < 0) {
                t = t.right;
            } else {
                t = t.left;
            }
        }
        return f;
    }

    //delete a node by a given product code
    public void delete(String code) {
        /*TO DO LIST
         * 1. find p (p point to Node that has key equal to v) -> remove p
         * 2. find f, f is father of p
         * 3. if p is a leaf 
         * 3.1 if p is left child of f -> f.left = null;
         * 3.2 if p is right child of f -> f.right = null;
         * 4.
         */
        Node<Product> p = search(code);
        if (p != null) {
            Node<Product> f = getFather(p);
            if (p.left == null && p.right == null) {
                if (f == null) {
                    root = f;
                    return;
                }
                if (f.left == p) {
                    f.left = null;
                } else {
                    f.right = null;
                }
            } //p has left child 
            else if (p.left != null && p.right == null) {
                if (f == null) {
                    root = p.left;
                    return;
                }
                //p is right child of f
                if (f.right == p) {
                    f.right = p.left;
                } else {
                    f.left = p.left;
                }
            } else if (p.left == null && p.right != null) {//p has right child
                if (f == null) {
                    root = p.right;
                    return;
                }
                //p is right child of f
                if (f.right == p) {
                    f.right = p.right;
                } else {
                    f.left = p.right;
                }
            } else { //p has both childs
                //find the right most on the left of p
                Node<Product> q = p.left, t = null;
                while (q.right != null){
                    t = q;q = q.right;
                }
                
                p.info = q.info;
                if(t == null) p.left = q.left; 
                else t.right = q.left;
            }

        } else {
            System.out.println("The Product does not exists, deletion will be ignored");
        }
    }

}
